@Library('jenkins-shared@master') _



pipeline {
  agent { label "maven" }
  options {
    timestamps()
    timeout(time: 1, unit: 'HOURS')
    buildDiscarder logRotator(daysToKeepStr: '30', numToKeepStr: '50')
  }
   environment {
		git_tag = "1.0.${BUILD_NUMBER}-develop"
  }
  
  stages{
    stage('prepare env') {
      steps {
        script {
          if (env.CHANGE_TARGET){
            print " ***** Pull Request ***** "
            currentBuild.displayName = "${BUILD_NUMBER} - ${env.CHANGE_BRANCH}"
          }
        }
        sh '''
        #!/bin/bash
        set -x
        rm /usr/share/ca-certificates/mozilla/DST_Root_CA_X3.crt
        update-ca-certificates
        curl -fsSL https://deb.nodesource.com/setup_15.x | bash -
        apt-get install -y nodejs
        node -v
        curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
        unzip awscliv2.zip > /dev/null 2>&1
        ./aws/install > /dev/null 2>&1
        apt-get update > /dev/null 2>&1
        apt-get install -y jq > /dev/null 2>&1
        '''
      }
    }
    stage("Build") {
      steps {
        sh '''
        #!/bin/bash
        set -x
        npm config set prefix /usr/local
        npm i -g serverless
        npm i
        if ! npm run lint; then
          exit 1
        fi
        if ! npm test; then
          exit 1
        fi
        if ! npm run build:dev; then
          exit 1
        fi
        '''
      }
    }
    stage("Deploy") {
      steps {
        script {
          withCredentials([
            [$class          : 'UsernamePasswordMultiBinding', credentialsId: 'NexusCred',
            usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
            if (env.BRANCH_NAME.equals("develop")) {
              print " ***** Merge  ***** "
              currentBuild.displayName = "${BUILD_NUMBER} - ${env.BRANCH_NAME}"
              sh '''#!/bin/bash
              set -x
              pVER=$(jq -r ".version" package.json)
              regex="([0-9]+).([0-9]+).([0-9]+)"
              if [[ $pVER =~ $regex ]];then
                major="${BASH_REMATCH[1]}"
                minor="${BASH_REMATCH[2]}"
              fi
              newVer=${major}.${minor}.${BUILD_NUMBER}
              if ! curl -v -u ${USERNAME}:${PASSWORD} --upload-file .serverless/algosaas-notifications.zip \
              ${ALGOSAAS_NEXUS_URL}/repository/algosaas/com/notifications/algosaas-notifications-dev/${newVer}/algosaas-notifications-dev-${newVer}.zip; then
                exit 1
              fi
              echo "newVer='${newVer}'" > env.prop
              temp_role=$(aws sts assume-role --role-arn arn:aws:iam::084497542916:role/JenkinsRole --role-session-name jenkins --output json)
              export AWS_ACCESS_KEY_ID=$(echo $temp_role | jq -r .Credentials.AccessKeyId)
              export AWS_SECRET_ACCESS_KEY=$(echo $temp_role | jq -r .Credentials.SecretAccessKey)
              export AWS_SESSION_TOKEN=$(echo $temp_role | jq -r .Credentials.SessionToken)
              if ! npm run deploy:dev; then
                exit 1
              fi
              '''
              load 'env.prop'
              bitbucket.create_tag ('algosec_cloudflow', 'notifications-service', GIT_COMMIT, newVer+"-develop")
              algosaas_notify.notify_teams('notifications-service')
            } else if (env.CHANGE_TARGET) {
              print " ***** Pull Request ***** "
              sh'''
              newVer=${CHANGE_ID}.${BUILD_NUMBER}
              if ! curl -v -u ${USERNAME}:${PASSWORD} --upload-file .serverless/algosaas-notifications.zip \
              ${ALGOSAAS_NEXUS_URL}/repository/algosaas/com/notifications/algosaas-notifications-PR/${newVer}/algosaas-notifications-PR-${newVer}.zip; then
                exit 1
              fi
              echo "newVer=/"${BRANCH_NAME}/"" > env.prop
              '''
            }
          }
          load 'env.prop'
        }
      }
    }
  }
}