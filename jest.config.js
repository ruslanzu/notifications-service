// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  // Automatically clear mock calls and instances between every test
  clearMocks: true,
  // Indicates whether the coverage information should be collected while executing the test
  collectCoverage: true,
  // A list of reporter names that Jest uses when writing coverage reports
  coverageReporters: ['text', 'html', 'json'],
  // Use this configuration option to add custom reporters to Jest
  reporters: ['default', 'jest-junit'],
  // A preset that is used as a base for Jest's configuration
  preset: 'ts-jest',
  // The test environment that will be used for testing
  testEnvironment: 'node',
  // The glob patterns Jest uses to detect test files
  testMatch: ['**/spec/**/*.spec.ts'],
  collectCoverageFrom: [
    "src/**/*.ts"
  ],
  moduleNameMapper: {

  },
};
