import { SQSEvent} from 'aws-lambda';
import { getLoggerService } from '../services/logger.service';
import { DynamoDBConnectionsService } from "../services/dynamodb-connections.service";
import { NotificationRequest, NotificationRequestSchema } from "../../models/notification-request";
import { GeneralUtils } from "../utils/general";
import { NotificationsService } from "../services/notifications.service";
import { NotificationMessage } from "../../models/notification-message";

const logger = getLoggerService();
const notificationService: NotificationsService = new NotificationsService(logger);

export async function sendNotification(event: SQSEvent, context: any) {
  logger.info('Send notification - event', event);
  for (const record of event.Records) {
    logger.info(`Start processing notification request: ${record.body}`);
    try {
      const notificationRequest: NotificationRequest = GeneralUtils.getBodyObject(record.body, NotificationRequestSchema);
      const openConnections = await DynamoDBConnectionsService.findOpenConnections(notificationRequest);
      logger.info(`Fetched ${openConnections.length} open connections`);
      if (openConnections.length > 0) {
        const notificationMessage: NotificationMessage = NotificationsService.notificationRequestToMessage(notificationRequest);
        await Promise.all(openConnections.map((connectionId) => notificationService.sendNotification(connectionId, notificationMessage)));
      }
      logger.info(`Finished processing notification request`);
    } catch (e) {
      logger.error(`Failed processing notification request: ${e.message}`);
    }
  }
  return `Finished processing ${event.Records.length} notification requests`;
}
