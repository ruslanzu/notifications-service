import { StatusCodes } from 'http-status-codes';
import { APIGatewayEvent, APIGatewayProxyResult } from 'aws-lambda';
import { getLoggerService } from '../services/logger.service';
import { RequestUtils } from "../utils/request-utils";
import { DynamoDBConnectionsService } from "../services/dynamodb-connections.service";
import {ConnectionInfo} from "../../models/connection-info";
import { GeneralUtils } from "../utils/general";

const logger = getLoggerService();

function extractConnectionInfoFromEvent(event: APIGatewayEvent): ConnectionInfo {
  const connectionId = GeneralUtils.extractValueOrThrow(event.requestContext, 'connectionId');
  const username = GeneralUtils.extractValueOrThrow(event.requestContext?.authorizer, 'username');
  const tenantId = GeneralUtils.extractValueOrThrow(event.requestContext?.authorizer, 'tenantId');
  const product = GeneralUtils.extractValueOrThrow(event.queryStringParameters, 'product');
  return { connectionId, username, tenantId, product };
}

export async function onConnect(event: APIGatewayEvent, context: any): Promise<APIGatewayProxyResult> {
  logger.info('Connect - event', event);
  let connectionInfo;
  try {
    connectionInfo = extractConnectionInfoFromEvent(event);
    logger.info(`Adding new connection: ${JSON.stringify(connectionInfo)}`);
    await DynamoDBConnectionsService.addConnection(connectionInfo);
    return RequestUtils.getResponseObject(StatusCodes.OK, {message: 'connected'});
  } catch (e) {
    if (!connectionInfo) {
      logger.error(`Missing required parameter - ${e}`);
      return RequestUtils.getResponseObject(StatusCodes.BAD_REQUEST, {message: e.message});
    } else {
      logger.error(`Failed saving new connection, ${JSON.stringify(connectionInfo)} - ${e}`);
      return RequestUtils.getResponseObject(StatusCodes.INTERNAL_SERVER_ERROR);
    }
  }
}

export async function onDisconnect(event: APIGatewayEvent, context: any): Promise<APIGatewayProxyResult> {
  logger.info('Disconnect - event', event);
  let connectionId;
  try {
    connectionId = GeneralUtils.extractValueOrThrow(event.requestContext, 'connectionId');
    logger.info(`Deleting disconnected connectionId: ${connectionId}`);
    await DynamoDBConnectionsService.removeConnection(connectionId);
    return RequestUtils.getResponseObject(StatusCodes.OK);
  } catch (e) {
    if (!connectionId) {
      logger.error(`Missing required parameter - ${e}`);
      return RequestUtils.getResponseObject(StatusCodes.BAD_REQUEST, {message: e.message});
    } else {
      logger.error(`Failed removing connectionId: ${connectionId} - ${e}`);
      return RequestUtils.getResponseObject(StatusCodes.INTERNAL_SERVER_ERROR);
    }
  }
}
