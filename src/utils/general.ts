import { Schema } from "joi";

export const WEBSOCKET_SUBPROTOCOL_HEADER = 'Sec-WebSocket-Protocol';

export class GeneralUtils {
  public static extractValueOrThrow(object: any, attribute: string) {
    if (!object || !object[attribute]) {
      throw new Error(`${attribute} is missing`);
    }
    return object[attribute];
  }

  public static getBodyObject<T>(body: string, schema: Schema): T {
    let input: T;
    try {
      input = JSON.parse(body);
    } catch (e) {
      throw new Error('Error parsing body to json');
    }
    return GeneralUtils.validateObject(input, schema);
  }

  private static validateObject<T>(input: any, schema: Schema): T {
    const error = schema.validate(input).error;
    if (error) {
      throw new Error(error.message);
    }
    return input;
  }
}
