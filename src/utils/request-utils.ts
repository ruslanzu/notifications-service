import {HttpStatus} from "aws-sdk/clients/lambda";
import {APIGatewayProxyResult} from "aws-lambda";
import {WEBSOCKET_SUBPROTOCOL_HEADER} from "./general";

export const defaultHeaders: { [headerName: string]: string | number | boolean } = {
  'Access-Control-Allow-Credentials': true,
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

export class RequestUtils {
  public static getResponseObject(statusCode: HttpStatus, body?: any): APIGatewayProxyResult {
    const headers: { [header: string]: boolean | number | string } = {
      ...defaultHeaders,
      [WEBSOCKET_SUBPROTOCOL_HEADER]: 'Bearer'
    };
    return {
      statusCode,
      headers,
      body: body ? JSON.stringify(body) : '',
    };
  }
}
