import { ApiGatewayManagementApi } from "aws-sdk";
import { NotificationMessage } from "../../models/notification-message";
import { NotificationRequest } from "../../models/notification-request";
import { LoggerService } from "./logger.service";

export class NotificationsService {
  private apiGwManagementApi: ApiGatewayManagementApi;

  constructor(private readonly logger: LoggerService) {
    this.apiGwManagementApi = new ApiGatewayManagementApi({
      apiVersion: '2018-11-29',
      endpoint: process.env.endpoint_uri
    });
  }

  public async sendNotification(connectionId: string, message: NotificationMessage) {
    const params: ApiGatewayManagementApi.PostToConnectionRequest = {
      ConnectionId: connectionId,
      Data: JSON.stringify(message)
    };

    return this.apiGwManagementApi.postToConnection(params)
      .promise()
      .then(()=> true)
      .catch((e) => {
        this.logger.error(`Failed sending notification to connectionId: ${connectionId}, error: ${e.message}`);
        throw new Error(`Failed sending notification to connectionId: ${connectionId}`);
      });
  }

  public static notificationRequestToMessage(notificationRequest: NotificationRequest): NotificationMessage {
    const {topic, body} = notificationRequest;
    return {topic, body};
  }
}
