import { DynamoDB } from 'aws-sdk';
import { ConnectionInfo } from "../../models/connection-info";
import { DocumentClient } from "aws-sdk/lib/dynamodb/document_client";
import { NotificationRequest } from "../../models/notification-request";

const dynamoDB = new DynamoDB.DocumentClient();
const connectionsTableName = 'algosaas-websocket-connections';

export class DynamoDBConnectionsService {
  public static async addConnection(connectionInfo: ConnectionInfo): Promise<void> {
    const Item: ConnectionInfo = {
      ...connectionInfo,
      timestamp: new Date().toISOString()
    };

    const putParams: DocumentClient.PutItemInput = {
      TableName: connectionsTableName,
      Item
    };
    await dynamoDB.put(putParams).promise();
  }

  public static async removeConnection(connectionId: string): Promise<void> {
    const deleteParams: DocumentClient.DeleteItemInput = {
      TableName: connectionsTableName,
      Key: { connectionId }
    };
    await dynamoDB.delete(deleteParams).promise();
  }

  public static async findOpenConnections(notificationRequest: NotificationRequest): Promise<string[]> {
    let params: DocumentClient.ScanInput = {
      TableName: connectionsTableName,
      ProjectionExpression: 'connectionId',
    }
    if (this.needToAddFilter(notificationRequest)) {
      params = {
        ...params,
        ...this.buildFilterExpressionAndValues(notificationRequest)
      };
    }
    return dynamoDB.scan(params).promise().then(result => {
      if (!result.Items) {
        return [];
      } else {
        return result.Items.map(i => i.connectionId);
      }
    })
  }

  private static needToAddFilter(notificationRequest: NotificationRequest) {
    return notificationRequest.tenantId || notificationRequest.username || notificationRequest.product;
  }

  private static concatFilterExpression(filterExpression: string, paramName: string) {
    let result = filterExpression;
    if (filterExpression) {
      result += 'AND ';
    }
    return `${result} ${paramName} = :${paramName} `;
  }

  private static buildFilterExpressionAndValues(notificationRequest: NotificationRequest) {
    let filterExpression = '';
    const expressionAttributeValues = {};

    if (notificationRequest.tenantId) {
      filterExpression = this.concatFilterExpression(filterExpression, 'tenantId');
      expressionAttributeValues[':tenantId'] = notificationRequest.tenantId;
    }
    if (notificationRequest.username) {
      filterExpression = this.concatFilterExpression(filterExpression, 'username');
      expressionAttributeValues[':username'] = notificationRequest.username;
    }
    if (notificationRequest.product) {
      filterExpression = this.concatFilterExpression(filterExpression, 'product');
      expressionAttributeValues[':product'] = notificationRequest.product;
    }
    return {
      FilterExpression: filterExpression,
      ExpressionAttributeValues: expressionAttributeValues
    }
  }
}
