import { Logger as WinstonLogger } from 'winston';
import * as winston from 'winston';

type RequestContext = { [key: string]: string | number };

export enum LogLevel {
  ERROR = 'error',
  WARN = 'warning',
  INFO = 'info',
  DEBUG = 'debug',
  ADMIN = 'info',
}

export class LoggerService {
  private logger: WinstonLogger;
  private requestContext: RequestContext | undefined;

  constructor(logLevel: string) {
    this.logger = winston.createLogger({
      level: logLevel,
      format: winston.format.combine(winston.format.timestamp(), winston.format.json()),
      transports: [new winston.transports.Console()],
    });
  }

  public setContext(requestContext: RequestContext) {
    this.requestContext = requestContext;
  }

  public info(message: string, meta?: any | any[]): void {
    this.logger.info(message, { ...this.getDefaultLoggerParams(), ...meta });
  }

  public debug(message: string, meta?: any | any[]): void {
    this.logger.debug(message, { ...this.getDefaultLoggerParams(), ...meta });
  }

  public warn(message: string, meta?: any | any[]): void {
    this.logger.warn(message, { ...this.getDefaultLoggerParams(), ...meta });
  }

  public error(message: string, meta?: any | any[]): void {
    this.logger.error(message, { ...this.getDefaultLoggerParams(), ...meta });
  }
  private getDefaultLoggerParams(): {[id:string]:any} {
    if (this.requestContext) {
      return this.requestContext;
    }
    return {};
  }
}

export function getLoggerService(): LoggerService {
  const logLevel = process.env.logLevel || LogLevel.INFO;
  return new LoggerService(logLevel);
}
