export interface NotificationMessage {
  topic: string,
  body?: any
}
