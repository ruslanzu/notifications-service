export interface ConnectionInfo {
  connectionId: string;
  tenantId: string;
  username: string;
  product: string;
  timestamp?: string;
}
