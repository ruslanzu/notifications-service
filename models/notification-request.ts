import Joi from "joi";

export interface NotificationRequest {
  topic: string,
  tenantId?: string,
  username?: string,
  product?: string,
  body?: string
}

export const NotificationRequestSchema = Joi.object({
  topic: Joi.string().required().error(new Error('Invalid value in topic field')),
  tenantId: Joi.when('username', {
    is: Joi.exist(),
    then: Joi.string().required().error(new Error('Invalid value in tenantId field')),
    otherwise: Joi.string().optional().error(new Error('Invalid value in tenantId field'))
  }),
  username: Joi.string().optional().error(new Error('Invalid value in username field')),
  product: Joi.string().optional().error(new Error('Invalid value in product field')),
  body: Joi.any(),
});
