import { StatusCodes } from 'http-status-codes';
import { createAPIGatewayProxyEvent, createContext, mockSuccessTestResult, mockFailTestResult} from "../util";
import { onConnect } from "../../src/lambda/connections";

let errors: string[] = [];
jest.mock("winston", () => {
  return {
    format: {
      combine: jest.fn(),
      timestamp: jest.fn(),
      json: jest.fn()
    },
    createLogger: jest.fn().mockReturnValue({

      debug: jest.fn(),
      log: jest.fn(),
      info: jest.fn(),
      error: jest.fn((message: any) => {errors.push(message)})
    }),
    transports: {
      Console: jest.fn()
    }
  }
});

let mockDynamoDBPut = () => Promise.resolve();
jest.mock('aws-sdk', () => {
  return {
    DynamoDB: {
      DocumentClient: jest.fn().mockImplementation(function () {
        return {
          put: () => {
            return {
              promise: mockDynamoDBPut
            }
          }
        }
      })
    }
  };
});

beforeEach(() => {
  jest.clearAllMocks();
  errors = [];
});

describe('Test onConnect lambda', () => {
  it('Fail: missing connectionId', async () => {
    const event = createAPIGatewayProxyEvent();
    const result = await onConnect(event, createContext());

    expect(errors).toContain('Missing required parameter - Error: connectionId is missing');

    expect(result).toEqual(mockFailTestResult(
      StatusCodes.BAD_REQUEST,
      JSON.stringify({message: 'connectionId is missing'})
    ));
  });

  it('Fail: missing username', async () => {
    const authorizerContext = {
      tenantId: '123'
    };
    const queryParams = {
      product: 'cloudflow'
    };
    const event = createAPIGatewayProxyEvent('12345', authorizerContext, queryParams);
    const result = await onConnect(event, createContext());

    expect(errors).toContain('Missing required parameter - Error: username is missing');

    expect(result).toEqual(mockFailTestResult(
      StatusCodes.BAD_REQUEST,
      JSON.stringify({message: 'username is missing'})
    ));
  });

  it('Fail: missing tenantId', async () => {
    const authorizerContext = {
      username: 'dummy'
    };
    const queryParams = {
      product: 'cloudflow'
    };
    const event = createAPIGatewayProxyEvent('12345', authorizerContext, queryParams);
    const result = await onConnect(event, createContext());

    expect(errors).toContain('Missing required parameter - Error: tenantId is missing');

    expect(result).toEqual(mockFailTestResult(
      StatusCodes.BAD_REQUEST,
      JSON.stringify({message: 'tenantId is missing'})
    ));
  });

  it('Fail: missing product', async () => {
    const authorizerContext = {
      username: 'dummy',
      tenantId: '123',
    };
    const event = createAPIGatewayProxyEvent('12345', authorizerContext, {});
    const result = await onConnect(event, createContext());

    expect(errors).toContain('Missing required parameter - Error: product is missing');

    expect(result).toEqual(mockFailTestResult(
      StatusCodes.BAD_REQUEST,
      JSON.stringify({message: 'product is missing'})
    ));
  });

  it('Fail: error in dynamoDB put', async () => {
    const authorizerContext = {
      username: 'dummy',
      tenantId: '123',
    };
    const queryParams = {
      product: 'cloudflow'
    };
    mockDynamoDBPut = () => Promise.reject('dynamoDB error');
    const event = createAPIGatewayProxyEvent('12345', authorizerContext, queryParams);
    const result = await onConnect(event, createContext());

    const connectionInfo = {
      connectionId: '12345',
      ...authorizerContext,
      ...queryParams
    }
    expect(errors).toContain(`Failed saving new connection, ${JSON.stringify(connectionInfo)} - dynamoDB error`);

    expect(result).toEqual(mockFailTestResult(StatusCodes.INTERNAL_SERVER_ERROR));
  });

  it('Success: connection saved', async () => {
    const authorizerContext = {
      username: 'dummy',
      tenantId: '123',
    };
    const queryParams = {
      product: 'cloudflow'
    };
    const header = 'Bearer, 12345';
    mockDynamoDBPut = () => Promise.resolve();
    const event = createAPIGatewayProxyEvent('12345', authorizerContext, queryParams, header);
    const result = await onConnect(event, createContext());
    expect(result).toEqual(mockSuccessTestResult(JSON.stringify({message: 'connected'})));
  })
});
