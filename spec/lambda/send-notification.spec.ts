import { createSQSEvent, createContext } from "../util";
import { sendNotification } from "../../src/lambda/notifications";

let infos: string[] = [];
let errors: string[] = [];
jest.mock("winston", () => {
  return {
    format: {
      combine: jest.fn(),
      timestamp: jest.fn(),
      json: jest.fn()
    },
    createLogger: jest.fn().mockReturnValue({

      debug: jest.fn(),
      log: jest.fn(),
      info: jest.fn((message: any) => {infos.push(message)}),
      error: jest.fn((message: any) => {errors.push(message)})
    }),
    transports: {
      Console: jest.fn()
    }
  }
});

let mockDynamoDBScan = () => Promise.resolve({} as any);
let mockPostToConnection = () => Promise.resolve({} as any);
jest.mock('aws-sdk', () => {
  return {
    DynamoDB: {
      DocumentClient: jest.fn().mockImplementation(function () {
        return {
          scan: () => {
            return {
              promise: mockDynamoDBScan
            }
          }
        }
      })
    },
    ApiGatewayManagementApi: jest.fn().mockImplementation(function () {
      return {
        postToConnection: () => {
          return {
            promise: mockPostToConnection
          }
        }
      }
    })
  };
});

beforeEach(() => {
  jest.clearAllMocks();
  errors = [];
  infos = [];
});

describe('Test sendNotification lambda', () => {
  it('Fail: missing topic in record.body', async () => {
    const event = createSQSEvent({});
    await sendNotification(event, createContext());
    expect(errors).toContain('Failed processing notification request: Invalid value in topic field');
  });

  it('Fail: missing tenantId in record.body when username exists', async () => {
    const event = createSQSEvent({topic: 'UPDATE', username: 'dummy'});
    await sendNotification(event, createContext());
    expect(errors).toContain('Failed processing notification request: Invalid value in tenantId field');
  });

  it('Fail: dynamoDB scan failure', async () => {
    const event = createSQSEvent({topic: 'UPDATE'});
    mockDynamoDBScan = () => Promise.reject(new Error('dynamodb error'));
    await sendNotification(event, createContext());
    expect(errors).toContain('Failed processing notification request: dynamodb error');
  });

  it('Fail: post to connection error', async () => {
    const event = createSQSEvent({topic: 'UPDATE'});
    mockDynamoDBScan = () => Promise.resolve({Items: [{connectionId:'connId'}]});
    mockPostToConnection = () => Promise.reject(new Error('post_to_connection error'));
    const result = await sendNotification(event, createContext());
    expect(infos).toContain('Fetched 1 open connections');
    expect(errors).toContain('Failed sending notification to connectionId: connId, error: post_to_connection error');
    expect(result).toEqual('Finished processing 1 notification requests');
  });

  it('Success: no open connections found', async () => {
    const event = createSQSEvent({topic: 'UPDATE'});
    mockDynamoDBScan = () => Promise.resolve({});
    await sendNotification(event, createContext());
    expect(infos).toContain('Fetched 0 open connections');
  });

  it('Success: send notification - one connection', async () => {
    const event = createSQSEvent({topic: 'UPDATE'});
    mockDynamoDBScan = () => Promise.resolve({Items: [{connectionId:'connId'}]});
    mockPostToConnection = () => Promise.resolve({});
    const result = await sendNotification(event, createContext());
    expect(infos).toContain('Fetched 1 open connections');
    expect(errors.length).toEqual(0);
    expect(result).toEqual('Finished processing 1 notification requests');
  });

  it('Success: send notification - few connections', async () => {
    const event = createSQSEvent({topic: 'UPDATE'});
    mockDynamoDBScan = () => Promise.resolve({Items: [{connectionId:'connId1'}, {connectionId: 'connId2'}]});
    mockPostToConnection = () => Promise.resolve({});
    const result = await sendNotification(event, createContext());
    expect(infos).toContain('Fetched 2 open connections');
    expect(errors.length).toEqual(0);
    expect(result).toEqual('Finished processing 1 notification requests');
  });
});
