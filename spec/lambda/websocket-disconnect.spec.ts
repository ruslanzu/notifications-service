import { StatusCodes } from 'http-status-codes';
import { createAPIGatewayProxyEvent, createContext, mockSuccessTestResult, mockFailTestResult} from "../util";
import { onDisconnect } from "../../src/lambda/connections";

let errors: string[] = [];
jest.mock("winston", () => {
  return {
    format: {
      combine: jest.fn(),
      timestamp: jest.fn(),
      json: jest.fn()
    },
    createLogger: jest.fn().mockReturnValue({

      debug: jest.fn(),
      log: jest.fn(),
      info: jest.fn(),
      error: jest.fn((message: any) => {errors.push(message)})
    }),
    transports: {
      Console: jest.fn()
    }
  }
});

let mockDynamoDBDelete = () => Promise.resolve();
jest.mock('aws-sdk', () => {
  return {
    DynamoDB: {
      DocumentClient: jest.fn().mockImplementation(function () {
        return {
          delete: () => {
            return {
              promise: mockDynamoDBDelete
            }
          }
        }
      })
    }
  };
});

beforeEach(() => {
  jest.clearAllMocks();
  errors = [];
});

describe('Test onDisconnect lambda', () => {
  it('Fail: missing connectionId', async () => {
    const event = createAPIGatewayProxyEvent();
    const result = await onDisconnect(event, createContext());

    expect(errors).toContain('Missing required parameter - Error: connectionId is missing');

    expect(result).toEqual(mockFailTestResult(
      StatusCodes.BAD_REQUEST,
      JSON.stringify({message: 'connectionId is missing'})
    ));
  });

  it('Fail: error in dynamoDB delete', async () => {
    mockDynamoDBDelete = () => Promise.reject('dynamoDB error');
    const event = createAPIGatewayProxyEvent('12345');
    const result = await onDisconnect(event, createContext());
    expect(errors).toContain('Failed removing connectionId: 12345 - dynamoDB error');
    expect(result).toEqual(mockFailTestResult(StatusCodes.INTERNAL_SERVER_ERROR));
  });

  it('Success: connection deleted', async () => {
    mockDynamoDBDelete = () => Promise.resolve();
    const event = createAPIGatewayProxyEvent('12345');
    const result = await onDisconnect(event, createContext());
    expect(result).toEqual(mockSuccessTestResult(''));
  })
});
