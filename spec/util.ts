import { APIGatewayProxyEvent } from 'aws-lambda/trigger/api-gateway-proxy';
import { APIGatewayEventIdentity } from 'aws-lambda/common/api-gateway';
import { APIGatewayProxyResult, SQSEvent } from 'aws-lambda';
import { defaultHeaders } from '../src/utils/request-utils';
import {WEBSOCKET_SUBPROTOCOL_HEADER} from "../src/utils/general";

export const mockSuccessTestResult = (body: string): APIGatewayProxyResult => ({
  body,
  statusCode: 200,
  headers: { ...defaultHeaders, [WEBSOCKET_SUBPROTOCOL_HEADER]: 'Bearer' },
});

export const mockFailTestResult = (statusCode: number, body = ''): APIGatewayProxyResult => ({
  body,
  statusCode,
  headers: { ...defaultHeaders, [WEBSOCKET_SUBPROTOCOL_HEADER]: 'Bearer' },
});

export const createSQSEvent = (record: any): SQSEvent => ({
  Records: [{
    messageId: '',
    receiptHandle: '',
    body: JSON.stringify(record),
    attributes: {
      ApproximateReceiveCount: '',
      SentTimestamp: '',
      SenderId: '',
      ApproximateFirstReceiveTimestamp: ''
    },
    messageAttributes: {},
    md5OfBody: '',
    eventSource: '',
    eventSourceARN: '',
    awsRegion: ''
  }]
});

export const createAPIGatewayProxyEvent = (connectionId?: string, authorizerContext?: any, queryStringParameters?: any, header?: string): APIGatewayProxyEvent => ({
  headers: {
    [WEBSOCKET_SUBPROTOCOL_HEADER]: header
  },
  httpMethod: '',
  isBase64Encoded: false,
  multiValueHeaders: {},
  multiValueQueryStringParameters: {},
  path: '',
  pathParameters: {},
  queryStringParameters: queryStringParameters || {},
  requestContext: {
    connectionId: connectionId,
    accountId: '',
    apiId: '',
    authorizer: authorizerContext || {},
    protocol: '',
    httpMethod: '',
    identity: apiGatewayEventIdentity(),
    path: '',
    stage: '',
    requestId: '',
    requestTimeEpoch: 0,
    resourceId: '',
    resourcePath: '',
  },
  resource: '',
  stageVariables: {},
  body: '',
});

export const createContext = () => ({
  awsRequestId: '',
  callbackWaitsForEmptyEventLoop: false,
  functionName: '',
  functionVersion: '',
  invokedFunctionArn: '',
  logGroupName: '',
  logStreamName: '',
  memoryLimitInMB: '',
  done(error?: Error, result?: any): void {
    return error ? error : result;
  },
  fail(error: Error | string): void {
    error = error ? error : '';
  },
  getRemainingTimeInMillis(): number {
    return 0;
  },
  succeed(messageOrObject: any | string, x?: any): void {
    messageOrObject = messageOrObject ? messageOrObject : x;
  },
});

const apiGatewayEventIdentity = (): APIGatewayEventIdentity => ({
  accessKey: '',
  accountId: '',
  apiKey: '',
  apiKeyId: '',
  caller: '',
  clientCert: null,
  cognitoAuthenticationProvider: '',
  cognitoAuthenticationType: '',
  cognitoIdentityId: '',
  cognitoIdentityPoolId: '',
  principalOrgId: '',
  sourceIp: '',
  user: '',
  userAgent: '',
  userArn: '',
});

